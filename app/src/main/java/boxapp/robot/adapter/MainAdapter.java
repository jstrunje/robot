package boxapp.robot.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import boxapp.robot.MainActivity;
import boxapp.robot.R;
import boxapp.robot.model.Meal;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int CATEGORY = 1;
    private static final int MEAL = 2;

    private ArrayList<Meal> meals;
    private Context context;

    public MainAdapter(ArrayList<Meal> meals, Context context) {
        this.meals = meals;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        Meal meal = meals.get(position);
        if (meal.getItemType() == Meal.ItemType.CATEGORY) {
            return CATEGORY;
        } else if (meal.getItemType() == Meal.ItemType.MEAL) {
            return MEAL;
        } else {
            return -1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CATEGORY) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_layout, parent, false);
            return new CategoryViewHolder(view);
        } else if (viewType == MEAL) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meal_layout, parent, false);
            return new MealViewHolder(view);
        } else {
            throw new RuntimeException("The type has to be ONE or TWO");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case CATEGORY:
                initCategoryLayout((CategoryViewHolder) holder, position);
                break;
            case MEAL:
                initMealsLayout((MealViewHolder) holder, position);
                break;
            default:
                break;
        }
    }

    private void initCategoryLayout(CategoryViewHolder holder, int pos) {
        holder.textView.setText(meals.get(pos).getName());
        switch (meals.get(pos).getName()) {
            case "Svakodnevna jela":
                holder.categoryLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.svakodnevna));
                break;
            case "Jela s grilla":
                holder.categoryLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.grill));
                break;
            case "Pohana jela":
                holder.categoryLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.pohana));
                break;
            case "Tjestenine":
                holder.categoryLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.tjestenine));
                break;
        }
    }

    private void initMealsLayout(final MealViewHolder holder, final int pos) {
        holder.name.setText(meals.get(pos).getName());
        holder.price.setText(String.valueOf(meals.get(pos).getPrice())+ ",00 kn");

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof MainActivity) {
                    ((MainActivity)context).addMealToOrder(meals.get(pos), (int) holder.count.getSelectedItemId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return meals == null ? 0 : meals.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout categoryLayout;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.category_textView);
            categoryLayout = (LinearLayout) itemView.findViewById(R.id.category_layout);
        }
    }

    public class MealViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView price;
        public Spinner count;
        public Button add;

        public MealViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.meal_name);
            price = (TextView) itemView.findViewById(R.id.meal_price);
            count = (Spinner) itemView.findViewById(R.id.meal_spinner);
            add = (Button) itemView.findViewById(R.id.meal_button);

            List<String> numbers = new ArrayList<String>();
            numbers.add("1");
            numbers.add("2");
            numbers.add("3");
            numbers.add("4");
            numbers.add("5");
            numbers.add("6");
            numbers.add("7");
            numbers.add("8");
            numbers.add("9");

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,  android.R.layout.simple_spinner_item, numbers);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            count.setAdapter(dataAdapter);
        }
    }
}
