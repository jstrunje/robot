package boxapp.robot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import boxapp.robot.MainActivity;
import boxapp.robot.OrderActivity;
import boxapp.robot.R;
import boxapp.robot.model.Meal;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {
    private ArrayList<Meal> meals = new ArrayList<>();
    private Context context;

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV;
        public TextView countTV;
        public TextView priceTV;
        public Button removeButton;

        public OrderViewHolder(View itemView) {
            super(itemView);
            nameTV = (TextView) itemView.findViewById(R.id.orderItem_name);
            countTV = (TextView) itemView.findViewById(R.id.orderItem_count);
            priceTV = (TextView) itemView.findViewById(R.id.orderItem_price);
            removeButton = (Button) itemView.findViewById(R.id.removeButton);
        }
    }

    public OrderAdapter(Context context, ArrayList<Meal> meals) {
        this.context = context;
        this.meals = meals;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
        return new OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        final Meal meal = meals.get(position);

        holder.nameTV.setText(meal.getName());
        holder.countTV.setText(String.valueOf(meal.getCount()));
        holder.priceTV.setText(String.valueOf(meal.getPrice() * meal.getCount()) + " kn");

        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof OrderActivity) {
                    ((OrderActivity)context).removeMeal(meal);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != meals ? meals.size() : 0);
    }


}
