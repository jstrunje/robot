package boxapp.robot;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import boxapp.robot.database.DataHelper;
import boxapp.robot.util.RobotPreferences;
import boxapp.robot.util.RobotPreferences.RobotPrefs;

public class SplashActivity extends Activity {
    private RobotPrefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        prefs = RobotPreferences.getPrefs(SplashActivity.this);

        if (prefs.firstRun) {
            addMealsToDB();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (prefs.isRegistred) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Intent intent = new Intent(SplashActivity.this, RegisterActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);
    }

    private void addMealsToDB() {
        DataHelper dataHelper = new DataHelper(SplashActivity.this);
        dataHelper.open();

        dataHelper.addMealToDB("Pečena teletina", 44,"svakodnevna");
        dataHelper.addMealToDB("Pečena svinjetina u umaku",35,"svakodnevna");
        dataHelper.addMealToDB("Grah s kobasicom",25,"svakodnevna");

        dataHelper.addMealToDB("Pileći file",25,"grill");
        dataHelper.addMealToDB("Pileći file punjen šunkom i sirom",29,"grill");
        dataHelper.addMealToDB("Pileći file omotan pancetom",28,"grill");
        dataHelper.addMealToDB("Pureći file",27,"grill");
        dataHelper.addMealToDB("Punjeni pureći file",32,"grill");
        dataHelper.addMealToDB("Svinjski kotleti",26,"grill");

        dataHelper.addMealToDB("Pileći file",25,"pohana");
        dataHelper.addMealToDB("Pureći file",28,"pohana");
        dataHelper.addMealToDB("Punjeni pileći file (šunka, sir)",30,"pohana");
        dataHelper.addMealToDB("Punjeni pureći file (šunka, sir)",34,"pohana");
        dataHelper.addMealToDB("Zagrebački odrezak",38,"pohana");

        dataHelper.addMealToDB("Tjestenina šunka, sir",28,"tjestenine");
        dataHelper.addMealToDB("Tjestenina s gorgonzolom",28,"tjestenine");
        dataHelper.addMealToDB("Tjestenina s povrćem",22,"tjestenine");
        dataHelper.addMealToDB("Špageti Bolognese",25,"tjestenine");
        dataHelper.addMealToDB("Domaći njoki Genovese",30,"tjestenine");
        dataHelper.addMealToDB("Domaći njoki s gorgonzolom",30,"tjestenine");
        dataHelper.addMealToDB("Domaći njoki s 4 vrste sira",30,"tjestenine");

        prefs.firstRun = false;
        RobotPreferences.savePrefs(SplashActivity.this, prefs);
    }
}
