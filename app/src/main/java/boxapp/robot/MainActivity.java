package boxapp.robot;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import boxapp.robot.adapter.MainAdapter;
import boxapp.robot.database.DataHelper;
import boxapp.robot.model.Meal;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Meal> order = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            order = (ArrayList<Meal>) intent.getExtras().getSerializable("order");
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.item_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        MainAdapter mainAdapter = new MainAdapter(setAdapterList(), MainActivity.this);
        recyclerView.setAdapter(mainAdapter);
    }

    public void addMealToOrder(Meal meal, int count) {
        for (Meal meal1 : order) {
            if (meal.getName().equals(meal1.getName())) {
                meal1.setCount(meal1.getCount() + count + 1);
                String mealAdded = getString(R.string.mealAdded) + meal.getName() + " (" + String.valueOf(meal.getCount()) + ")";
                Toast.makeText(this, mealAdded, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        meal.setCount(count + 1);
        order.add(meal);
        String mealAdded = getString(R.string.mealAdded) + meal.getName() + " (" + String.valueOf(meal.getCount()) + ")";
        Toast.makeText(this, mealAdded, Toast.LENGTH_SHORT).show();
    }

    private ArrayList<Meal> setAdapterList() {
        ArrayList<Meal> meals = new ArrayList<>();

        DataHelper dataHelper = new DataHelper(MainActivity.this);
        dataHelper.open();

        ArrayList<Meal> svakodnevnaJela = dataHelper.getAllMeals("svakodnevna");
        ArrayList<Meal> grillJela = dataHelper.getAllMeals("grill");
        ArrayList<Meal> pohanaJela = dataHelper.getAllMeals("pohana");
        ArrayList<Meal> tjestenine = dataHelper.getAllMeals("tjestenine");

        meals.add(new Meal(1, getCategories().get(0), 0, 0, Meal.ItemType.CATEGORY));
        meals.addAll(svakodnevnaJela);
        meals.add(new Meal(2, getCategories().get(1), 0, 0, Meal.ItemType.CATEGORY));
        meals.addAll(grillJela);
        meals.add(new Meal(3, getCategories().get(2), 0, 0, Meal.ItemType.CATEGORY));
        meals.addAll(pohanaJela);
        meals.add(new Meal(4, getCategories().get(3), 0, 0, Meal.ItemType.CATEGORY));
        meals.addAll(tjestenine);

        return meals;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

       if (id == R.id.menu_order) {
           if (order.isEmpty()) {
               Toast.makeText(this, "Niste dodali ni jedno jelo", Toast.LENGTH_SHORT).show();
           }
           else {
               Intent orderIntent = new Intent(MainActivity.this, OrderActivity.class);
               orderIntent.putExtra("order", order);
               startActivity(orderIntent);
           }
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<String> getCategories () {
        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Svakodnevna jela");
        arrayList.add("Jela s grilla");
        arrayList.add("Pohana jela");
        arrayList.add("Tjestenine");

        return arrayList;
    }
}
