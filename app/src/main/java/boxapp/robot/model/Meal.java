package boxapp.robot.model;

import java.io.Serializable;

public class Meal implements Serializable {
    public Meal() {

    }

    public enum ItemType {
        CATEGORY, MEAL;
    }

    private int id;
    private String name;
    private int price;
    private int count;
    private ItemType itemType;

    public Meal(int id, String name, int price, int count, ItemType itemType) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
        this.itemType = itemType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
}
