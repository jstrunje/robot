package boxapp.robot.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper {

    private	static	final	String	DATABASE_CREATE_SVAKODNEVNA	=	"create	table svakodnevna "
            +	"(id	INTEGER	PRIMARY	KEY	AUTOINCREMENT,	"
            +	"	name	TEXT	NOT	NULL,"
            +	"	price	INTEGER	NOT	NULL);";

    private	static	final	String	DATABASE_CREATE_GRILL	=	"create	table grill"
            +	"(id	INTEGER	PRIMARY	KEY	AUTOINCREMENT,	"
            +	"	name	TEXT	NOT	NULL,"
            +	"	price	INTEGER	NOT	NULL);";

    private	static	final	String	DATABASE_CREATE_POHANA	=	"create	table pohana"
            +	"(id	INTEGER	PRIMARY	KEY	AUTOINCREMENT,	"
            +	"	name	TEXT	NOT	NULL,"
            +	"	price	INTEGER	NOT	NULL);";

    private	static	final	String	DATABASE_CREATE_TJESTENINE	=	"create	table tjestenine"
            +	"(id	INTEGER	PRIMARY	KEY	AUTOINCREMENT,	"
            +	"	name	TEXT	NOT	NULL,"
            +	"	price	INTEGER	NOT	NULL);";

    public SqlHelper(Context context) {
        super(context, "robot.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SVAKODNEVNA);
        db.execSQL(DATABASE_CREATE_GRILL);
        db.execSQL(DATABASE_CREATE_POHANA);
        db.execSQL(DATABASE_CREATE_TJESTENINE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP	TABLE	IF	EXISTS	svakodnevna");
        db.execSQL("DROP	TABLE	IF	EXISTS	grill");
        db.execSQL("DROP	TABLE	IF	EXISTS	pohana");
        db.execSQL("DROP	TABLE	IF	EXISTS	tjestenine");

        onCreate(db);
    }
}
