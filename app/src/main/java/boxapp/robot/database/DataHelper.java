package boxapp.robot.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import boxapp.robot.model.Meal;

public class DataHelper {
    private SQLiteDatabase database;
    private	SqlHelper dbHelper;

    public DataHelper(Context context)	{
        dbHelper = new SqlHelper(context);
    }

    public void	open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void	close()	{
        database.close();
    }

    public void	addMealToDB(String name, int price, String table)	{
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("price",	price);
        database.insert(table, null, values);
    }

    public ArrayList<Meal> getAllMeals(String table) {
        ArrayList<Meal>	meals = new ArrayList<Meal>();

        Cursor cursor =	database.rawQuery("SELECT * FROM " + table,	null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Meal meal =	new	Meal();
            meal.setId(cursor.getInt(0));
            meal.setName(cursor.getString(1));
            meal.setPrice(cursor.getInt(2));
            meal.setCount(0);
            meal.setItemType(Meal.ItemType.MEAL);
            meals.add(meal);
            cursor.moveToNext();
        }
        cursor.close();
        return meals;
    }
}
