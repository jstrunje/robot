package boxapp.robot.util;

import android.content.Context;
import android.content.SharedPreferences;

public class RobotPreferences {

    public static RobotPrefs getPrefs(Context context) {
        RobotPrefs data = new RobotPrefs();

        SharedPreferences settings = context.getSharedPreferences("RobotPreferences", 0);
        data.firstRun = settings.getBoolean("firstrun", true);
        data.isRegistred = settings.getBoolean("isRegistred", false);
        data.address = settings.getString("address", null);
        data.name = settings.getString("name", null);
        data.email = settings.getString("email", null);
        data.phone = settings.getString("phone", null);

        return data;
    }

    public static void savePrefs(Context context, RobotPrefs data) {
        SharedPreferences settings = context.getSharedPreferences("RobotPreferences", 0);

        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean("firstrun", data.firstRun);
        editor.putBoolean("isRegistred", data.isRegistred);
        editor.putString("address", data.address);
        editor.putString("phone", data.phone);
        editor.putString("name", data.name);
        editor.putString("email", data.email);

        editor.apply();
    }

    public static class RobotPrefs {
        public boolean firstRun = true;
        public boolean isRegistred = false;
        public String address = null;
        public String phone = null;
        public String name = null;
        public String email = null;
    }
}
