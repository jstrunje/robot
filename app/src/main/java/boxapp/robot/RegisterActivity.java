package boxapp.robot;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import boxapp.robot.util.AppConfig;
import boxapp.robot.util.AppController;
import boxapp.robot.util.RobotPreferences;

public class RegisterActivity extends Activity {
    private EditText nameET, emailET, addressET, phoneET;
    private ProgressDialog progressDialog;
    private RobotPreferences.RobotPrefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button registerButton = (Button) findViewById(R.id.register_button);
        nameET = (EditText) findViewById(R.id.register_name);
        emailET = (EditText) findViewById(R.id.register_email);
        addressET = (EditText) findViewById(R.id.register_address);
        phoneET = (EditText) findViewById(R.id.register_phone);

        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setCancelable(false);

        prefs = RobotPreferences.getPrefs(RegisterActivity.this);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameET.getText().toString().trim();
                String email = emailET.getText().toString().trim();
                String address = addressET.getText().toString().trim();
                String phone = phoneET.getText().toString().trim();
                String deviceId = getDeviceID();

                if (!address.isEmpty() && !phone.isEmpty() && !name.isEmpty() && !email.isEmpty()) {
                    registerUser(name, email, address, phone, deviceId);
                } else {
                    Toast.makeText(RegisterActivity.this, "Molimo Vas unesite sve podatke!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void registerUser(final String name,  final String email, final String address, final String phone, final String deviceId) {
        progressDialog.setMessage("Registracija u tijeku...");
        showDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");

                    if (!error) {
                        prefs.name = name;
                        prefs.email = email;
                        prefs.address = address;
                        prefs.phone = phone;
                        prefs.isRegistred = true;
                        RobotPreferences.savePrefs(RegisterActivity.this, prefs);

                        Toast.makeText(RegisterActivity.this, "Registracija uspješna!", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        String errorMsg = jsonObject.getString("error_msg");
                        Toast.makeText(RegisterActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("address", address);
                params.put("phone", phone);
                params.put("deviceId", deviceId);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest, "req_register");
    }

    private String getDeviceID() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            return telephonyManager.getDeviceId();
        }
        else
            return "noID";
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
