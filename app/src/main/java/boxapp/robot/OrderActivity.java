package boxapp.robot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import boxapp.robot.adapter.OrderAdapter;
import boxapp.robot.model.Meal;
import boxapp.robot.util.AppConfig;
import boxapp.robot.util.AppController;
import boxapp.robot.util.RobotPreferences;

public class OrderActivity extends AppCompatActivity {
    private ArrayList<Meal> order;
    private ProgressDialog progressDialog;
    private RobotPreferences.RobotPrefs prefs;
    private EditText nameET, addressET, phoneET, emailET;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        getSupportActionBar().setTitle(getString(R.string.yourOrder));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        order = (ArrayList<Meal>) intent.getExtras().getSerializable("order");

        progressDialog = new ProgressDialog(OrderActivity.this);
        progressDialog.setCancelable(false);

        prefs = RobotPreferences.getPrefs(OrderActivity.this);

        nameET = (EditText) findViewById(R.id.order_name);
        nameET.setText(prefs.name);
        addressET = (EditText) findViewById(R.id.order_address);
        addressET.setText(prefs.address);
        phoneET = (EditText) findViewById(R.id.order_phone);
        phoneET.setText(prefs.phone);
        emailET = (EditText) findViewById(R.id.order_email);
        emailET.setText(prefs.email);
        Button orderButton = (Button) findViewById(R.id.orderButton);

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order(order);
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.order_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        OrderAdapter orderAdapter = new OrderAdapter(OrderActivity.this, order);
        recyclerView.setAdapter(orderAdapter);
    }

    private void order(ArrayList<Meal> meals) {
        String name = nameET.getText().toString().trim();
        String address = addressET.getText().toString().trim();
        String phone = phoneET.getText().toString().trim();
        String email = emailET.getText().toString().trim();
        String date = String.valueOf(Calendar.getInstance().getTime());
        int i = 0;
        int fin = meals.size();

        for (Meal meal : meals) {
            i++;
            boolean last = false;
            if (i == fin) {
                last = true;
            }
            orderFood(meal.getName(), String.valueOf(meal.getCount()), String.valueOf(fin), name, address, phone, email, date, last);
        }
    }

    private void orderFood(final String name, final String count, final String no, final String username, final String address, final String phone, final String email, final String time, final boolean last) {
        progressDialog.setMessage("Narudžba u tijeku...");
        showDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_ORDER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");

                    if (!error && last) {
                        order = null;
                        Toast.makeText(OrderActivity.this, "Narudžba uspješno poslana", Toast.LENGTH_SHORT).show();

                        prefs.name = username;
                        prefs.address = address;
                        prefs.phone = phone;
                        prefs.email = email;
                        RobotPreferences.savePrefs(OrderActivity.this, prefs);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(OrderActivity.this, MainActivity.class));
                            }
                        }, 1500);
                    } else {
                        String errorMsg = jsonObject.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("count", count);
                params.put("no", no);
                params.put("username", username);
                params.put("address", address);
                params.put("phone", phone);
                params.put("email", email);
                params.put("time", time);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest, "req_order");
    }

    public void removeMeal(Meal meal) {
        order.remove(meal);
        OrderAdapter orderAdapter = new OrderAdapter(OrderActivity.this, order);
        recyclerView.setAdapter(orderAdapter);
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(OrderActivity.this, MainActivity.class);
        intent.putExtra("order", order);
        startActivity(intent);
        super.onBackPressed();
    }
}
